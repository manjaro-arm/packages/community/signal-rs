# Maintainer: Dan Johansen <strit@manjaro.org>
# Contributor: Edgar Vincent <e-v@posteo.net>

pkgname=signal-rs
pkgver=0.0.0.r77.504c852
pkgrel=1
pkgdesc="A Rust-based Signal app with a QML/Kirigami frontend."
arch=('x86_64' 'aarch64')
url="https://sr.ht/~nicohman/signal-rs/"
license=('GPL3')
depends=('qt5-webengine')
makedepends=('git' 'cargo-nightly' 'qt5-webengine' 'kirigami2' 'gettext')
source=("git+https://git.sr.ht/~nicohman/signal-rs" "gettext-system.patch"
       "signal-rs.desktop")
sha256sums=('SKIP'
            '48c2854d620bf3805728a816043b218c7106c4d414e768368e25df37813a0a32'
            'ad3e71a3d6af08ee33c58c884bb097426b34aebe87add0f6fca57cf24d2864cd')

pkgver() {
  cd "$pkgname"
  printf "0.0.0.r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}

prepare() {
    cd "$pkgname"
    patch --strip=1 --input="${srcdir}/gettext-system.patch"
    export RUSTUP_TOOLCHAIN=nightly
    cargo fetch --locked --target "$CARCH-unknown-linux-gnu"
}

build() {
    cd "$pkgname"
    export RUSTUP_TOOLCHAIN=nightly
    export CARGO_TARGET_DIR=target
    cargo build --frozen --release --all-features
}

package() {
    install -Dm0755 -t "$pkgdir/usr/bin/" "$pkgname/target/release/$pkgname"
    install -Dm0644 -t "$pkgdir/usr/share/applications/" "${srcdir}/signal-rs.desktop"
}
